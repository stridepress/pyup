from flask import Flask, redirect, request, abort, session, render_template, url_for
from werkzeug.contrib.fixers import ProxyFix
from werkzeug.utils import secure_filename
from keycloak.realm import KeycloakRealm
from config import *
import paginate
import pickledb
import ipfsApi
import os

app = Flask(__name__)
app.secret_key = b'_1#y2L"F4Q8z\n\xec]/'
app.wsgi_app = ProxyFix(app.wsgi_app)

ipfs_client = ipfsApi.Client("ipfsapi.stride.press", 80)

app.config["UPLOAD_FOLDER"] = upload_folder

db = pickledb.load("namecache.db", True)

realm = KeycloakRealm(server_url=server, realm_name=realm)
oidc_client = realm.open_id_connect(client_id=client_id,
                                    client_secret=client_secret)

auth_url = oidc_client.authorization_url(
    redirect_uri=base_url + "/oauth", scope="openid")


def tmpl(t, **kwargs):
    return render_template("base.html", body=render_template(t, **kwargs))


def get_name(hash):
    if(db.get(hash)):
        return db.get(hash)
    ls = ipfs_client.ls(hash)
    if len(ls.get("Objects")) != 1:
        db.set(str(hash), str(hash))
        return hash
    if len(ls.get("Objects")[0].get("Links")) != 1:
        db.set(str(hash), str(hash))
        return hash
    name = ls.get("Objects")[0].get("Links")[0].get("Name")
    db.set(str(hash), name)
    return name


@app.route("/login")
def login():
    return redirect(auth_url)


@app.route("/oauth")
def oauth():
    code = request.args.get("code")
    if code is None:
        return abort(400)
    tok = oidc_client.authorization_code(
        code=code, redirect_uri=base_url + "/oauth")
    session["access_token"] = tok["access_token"]
    session["refresh_token"] = tok["refresh_token"]
    return redirect("/")


@app.route("/logout")
def logout():
    if "access_token" in session:
        oidc_client.logout(refresh_token=session["refresh_token"])
    session.pop("access_token", None)
    session.pop("refresh_token", None)
    return redirect("/")


@app.route("/", methods=["POST", "GET"])
def root():
    if request.method == "GET":
        if not "access_token" in session:
            return redirect("/login")
        try:
            info = oidc_client.userinfo(session["access_token"])
        except Exception as _:
            return redirect("/logout")
        return tmpl("index.html", userinfo=info)
    else:
        if not "access_token" in session:
            return abort(401)
        if "file" not in request.files:
            return abort(400)
        file = request.files["file"]
        if file.filename == "":
            return abort(400)
        filename = secure_filename(file.filename)
        path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
        file.save(path)
        res = ipfs_client.add(path, recursive=False)
        return redirect(ipfs_gateway + res[0]["Hash"])


@app.route("/page")
def page():
    pagenum = 1
    if request.args.get("p"):
        if int(request.args.get("p")):
            pagenum = int(request.args.get("p"))
    dat = ipfs_client.pin_ls(opts={"type": "recursive"})
    hashes = map((lambda(k, v): k), dat.get("Keys").iteritems())
    urls = map(
        (lambda k: {"url": ipfs_gateway + k, "name": get_name(k)}), hashes)
    p = paginate.Page(urls, page=pagenum, items_per_page=10,
                      item_count=len(urls))
    pattern = pattern = '$link_first $link_previous ~4~ $link_next $link_last (Page $page out of $page_count - total $item_count)'
    pagenav = p.pager(pattern, url=url_for("page") + "?p=$page")
    return tmpl("listing.html", nav=pagenav, elements=p.items)


if __name__ == "__main__":
    app.run(debug=True, threaded=True)
